//
//  PepperViewController.m
//  DegiMamagoto
//
//  Created by 中村太郎 on 2015/11/19.
//  Copyright © 2015年 中村太郎. All rights reserved.
//

#import "PepperViewController.h"
#import "SEManager.h"
#import <CoreMotion/CoreMotion.h>

@interface PepperViewController ()
@property (nonatomic) CMMotionManager *motionManager;
@end

@implementation PepperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.motionManager = [[CMMotionManager alloc] init];
    
    if(self.motionManager.accelerometerAvailable){
        self.motionManager.accelerometerUpdateInterval = 1/10.0f;
        CMAccelerometerHandler handler = ^(CMAccelerometerData *data,NSError *error){
            CGFloat accel = sqrt(data.acceleration.x*data.acceleration.x + data.acceleration.y*data.acceleration.y + data.acceleration.z * data.acceleration.y);
            if(accel > 1.5){
                [[SEManager sharedManager] playSound:@"pepper.mp3" loop:0];
            }
        };
        [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:handler];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
