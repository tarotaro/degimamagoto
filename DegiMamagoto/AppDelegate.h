//
//  AppDelegate.h
//  DegiMamagoto
//
//  Created by 中村太郎 on 2015/11/17.
//  Copyright © 2015年 中村太郎. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AppDelegateDelegate <NSObject>
-(void)soundBig;
@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, weak) id<AppDelegateDelegate> delegate;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) NSInteger fireSize;
@property (nonatomic) NSInteger ledSight;

@end

