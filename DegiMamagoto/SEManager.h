//
//  SEManager.h
//  DegiMamagoto
//
//  Created by 中村太郎 on 2015/11/20.
//  Copyright © 2015年 中村太郎. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SEManager : NSObject{
    NSMutableDictionary *soundDic;
}

@property(nonatomic) float soundVolume;

+ (SEManager *)sharedManager;
- (void)playSound:(NSString *)soundName loop:(NSInteger)loop;
- (void)stopSound:(NSString *)soundName;
-(void)setVolume:(NSString *)soundName volume:(float)volume;
@end
