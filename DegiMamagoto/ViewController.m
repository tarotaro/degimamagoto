//
//  ViewController.m
//  DegiMamagoto
//
//  Created by 中村太郎 on 2015/11/17.
//  Copyright © 2015年 中村太郎. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:0.0 forKey:@"rotation"];
    [defaults synchronize];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
