//
//  FireKnobViewController.m
//  DegiMamagoto
//
//  Created by 中村太郎 on 2015/11/19.
//  Copyright © 2015年 中村太郎. All rights reserved.
//

#import "FireKnobViewController.h"
#import "AppDelegate.h"
#import "SEManager.h"

@interface FireKnobViewController () <AppDelegateDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *KnobImageView;
@property (nonatomic) CGFloat rotation;
@property (nonatomic) BOOL isSoundBig;
@property (nonatomic) BOOL isRoast;

@end

static const CGFloat kDivideCoffie = 0.1;

@implementation FireKnobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragView:)];
    [self.KnobImageView addGestureRecognizer:pan];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.rotation = [defaults floatForKey:@"rotation"];
    AppDelegate *delegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    delegate.delegate = self;
    self.KnobImageView.transform = CGAffineTransformMakeRotation(self.rotation);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)soundBig{
    if(!self.isSoundBig){
        self.isSoundBig = YES;
    [[SEManager sharedManager] setVolume:@"roasting.mp3" volume:1.0];
    
    double delayInSeconds = 2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.isSoundBig = NO;
        [[SEManager sharedManager] setVolume:@"roasting.mp3" volume:fabs(self.rotation/(0.745*M_PI))*fabs(self.rotation/(0.745*M_PI))];
     });
    }
}

#pragma Private

-(void)dragView:(UIPanGestureRecognizer *)gesture{
    UIView *targetView = gesture.view;
    CGPoint p = [gesture translationInView:targetView];
    if(self.rotation == 0 && p.x < 0){
        [[SEManager sharedManager] playSound:@"firefirst.mp3" loop:0];
        [[SEManager sharedManager] playSound:@"firesecond.mp3" loop:0];

    }else if(self.rotation < -0.745*M_PI/4){
        if(!self.isRoast){
            self.isRoast = YES;
            [[SEManager sharedManager] playSound:@"roasting.mp3" loop:-1];
        }
    }
    if(self.isRoast){
        float fireSize = fabs(self.rotation/(0.745*M_PI))*fabs(self.rotation/(0.745*M_PI));
        [[SEManager sharedManager] setVolume:@"roasting.mp3" volume:fireSize];
        AppDelegate *delegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        delegate.fireSize = fireSize * 3 + 1;
    }
    self.rotation += M_PI/3 * (p.x/[UIScreen mainScreen].bounds.size.width)*kDivideCoffie;
    if(self.rotation > 0){
        self.rotation = 0;
        AppDelegate *delegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        delegate.fireSize = 0;
        [[SEManager sharedManager] stopSound:@"roasting.mp3"];
        self.isRoast = NO;
    }else if(self.rotation < -0.745 * M_PI){
        self.rotation = -0.745 * M_PI;
    }
    self.KnobImageView.transform = CGAffineTransformMakeRotation(self.rotation);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:self.rotation forKey:@"rotation"];
    [defaults synchronize];

}

@end
