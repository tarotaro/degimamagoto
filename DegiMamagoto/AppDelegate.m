//
//  AppDelegate.m
//  DegiMamagoto
//
//  Created by 中村太郎 on 2015/11/17.
//  Copyright © 2015年 中村太郎. All rights reserved.
//

#import "AppDelegate.h"
#import "SEManager.h"
#import <Konashi.h>
@interface AppDelegate ()
@property (nonatomic) NSTimer *requestTimer;
@property (nonatomic) NSTimer *ledTimer;

@property (nonatomic) float accelX;
@property (nonatomic) float accelY;
@property (nonatomic) BOOL isAccelX;
@property (nonatomic) BOOL isAccelY;
@property (nonatomic) BOOL isVoicePlay;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [Konashi initialize];
    
    [[Konashi shared] setReadyHandler:^{ [self ready];}];
    [[Konashi shared] setAnalogPinDidChangeValueHandler:^(KonashiAnalogIOPin pin, int value) {
        [self analogReadComplete:pin value:value];
    }];


    [Konashi find];
    self.requestTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                         target:self
                                                       selector:@selector(timerCallBack:)
                                                       userInfo:nil
                                                        repeats:YES];
    
    self.ledTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                         target:self
                                                       selector:@selector(ledCallBack:)
                                                       userInfo:nil
                                                        repeats:YES];
    self.ledSight = 0;
    self.isVoicePlay = NO;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void)ready{
    [Konashi pinMode:KonashiDigitalIO1 mode:KonashiPinModeOutput];
    [Konashi digitalWrite:KonashiDigitalIO1 value:KonashiLevelHigh];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"接続"
                                                    message:@"konashiに接続しました"
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    
    
    [alert show];


}

-(void)analogReadComplete:(KonashiAnalogIOPin) pin value:(int)value{
    if(pin == KonashiAnalogIO0){
        
        self.accelY = -(value - 130)/130.0f;
        self.isAccelY = YES;
        //NSLog(@"accelY %f", self.accelY);
        //NSLog(@"****accelY %d****",value);
    }else if(pin == KonashiAnalogIO2){

        self.accelX = -(value - 130)/130.0f;
        self.isAccelX = YES;
        //NSLog(@"****accelX %d*****",value);
        //NSLog(@"accelX %f", self.accelX);
    }
    
    if(self.isAccelX && self.isAccelY){
        float accelSize =sqrt(self.accelX*self.accelX + self.accelY*self.accelY);
        NSLog(@"***accel abs %f ***",accelSize);
        if(accelSize > 0.19 && self.fireSize){
            self.ledSight+=self.fireSize;
            NSLog(@"********Led ++++%d**************",self.ledSight);
            [self.delegate soundBig];
        }
    }

}

-(void)ledCallBack:(NSTimer *)timer{
    if(self.ledSight > 60){
        NSLog(@"********Light On**********");
        [Konashi digitalWrite:KonashiDigitalIO1 value:KonashiLevelLow];
    }
    if(self.ledSight > 68 && (!self.isVoicePlay)){
        [[SEManager sharedManager] playSound:@"voice.wav" loop:0];
        self.isVoicePlay = YES;
    }
}

-(void)timerCallBack:(NSTimer *)timer{
    [Konashi analogReadRequest:KonashiAnalogIO0];
    [Konashi analogReadRequest:KonashiAnalogIO2];
}

@end
