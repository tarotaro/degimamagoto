//
//  SEManager.m
//  DegiMamagoto
//
//  Created by 中村太郎 on 2015/11/20.
//  Copyright © 2015年 中村太郎. All rights reserved.
//

#import "SEManager.h"
#import <AVFoundation/AVFoundation.h>

@implementation SEManager


static SEManager *sharedData_ = nil;

+ (SEManager *)sharedManager{
    @synchronized(self){
        if (!sharedData_) {
            sharedData_ = [[SEManager alloc]init];
        }
    }
    return sharedData_;
}

- (id)init
{
    self = [super init];
    if (self) {
        soundDic = [NSMutableDictionary dictionary];
        _soundVolume = 1.0;
    }
    return self;
}

- (void)playSound:(NSString *)soundName loop:(NSInteger)loop{
    if([soundDic objectForKey:soundName] != nil)return;
    NSString *soundPath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:soundName];
    NSURL *urlOfSound = [NSURL fileURLWithPath:soundPath];
    
    AVAudioPlayer* player = [[AVAudioPlayer alloc] initWithContentsOfURL:urlOfSound error:nil];
    [player setNumberOfLoops:loop];
    player.volume = _soundVolume;
    player.delegate = (id)self;
    [soundDic setObject:player forKey:soundName];
    [player prepareToPlay];
    [player play];
}

-(void)stopSound:(NSString *)soundName{
    [[soundDic objectForKey:soundName] stop];
    [soundDic setValue:nil forKey:soundName];
    
}

-(void)setVolume:(NSString *)soundName volume:(float)volume{
    [[soundDic objectForKey:soundName] setVolume:volume];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    for(NSString *key in soundDic){
        if([soundDic objectForKey:key] == player){
            [soundDic setValue:nil forKey:key];
            return;
        }
    }
}

@end
